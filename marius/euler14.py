

def collatzLength(n):
  nSteps = 0
  while True:
    if n == 1: 
      return nSteps + 1
    elif n % 2 == 0: 
      n = n / 2
      nSteps += 1
    else:
      n = 3 * n + 1
      nSteps += 1


maxLength = 0
maxNumber = 0

for n in range(5, 1000000):
  length = collatzLength(n)
  if length > maxLength:
    maxNumber = n
    maxLength = length

print(maxNumber)
